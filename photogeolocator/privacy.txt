Photogeolocator Privacy Policy
This privacy policy designates how any information you give us while using the app (Photogeolocator) is used and protected.

Photogeolocator is strongly committed to protecting the privacy of its user. The app will never collect or share your personal information for any reason.

COLLECTION AND USE OF INFORMATION

    Photogeolocator does not collect any personal or non-personal information and therefore nobody will disclose any information.
    If you contact us for any reason, we may obtain and maintain any information you choose to provide, including your name, contact info such as your email address, as well as information about your mobile device or personal computer such as its device type, OS type, and logs. We will use this information to address and investigate the issues you have raised, only to provide you support.
